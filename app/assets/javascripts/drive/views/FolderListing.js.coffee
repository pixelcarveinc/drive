#= require drive/namespace
#= require pxcv/views/utils/FileUpload


class window.chocolate.drive.views.FolderListing extends chocolate.style.views.Listing

  events:
    'click #controls .delete': '_delete'
    'click #controls .move':   '_move'

  initialize: ->
    super

    @id = @$el.data('id')

    if @$('#new_file').length
      @fileUpload = new pxcv.views.utils.FileUpload
        el: @$('#new_file')
        upload:
          dataType: 'html'
          formData: [
            {name: 'folder_id', value: @$('#file_folder_id').val()}
          ]
      @listenTo @fileUpload, 'upload', @_fileUpload

    @uploads  = []

  # clean up sub-views
  cleanUp: ->
    super
    @fileUpload?.remove()
    _.each @uploads, (up) -> up.remove()

  multiDelete: (folders = [], files = []) ->
    # dynamically build a form for submitting items to delete
    # could be done using Backbone.Model, but this method will utilize
    # turbolinks and rails for UI rendering
    form = $('<form></form>')
      .attr('method', 'post')
      .attr('action', @options.paths.folders)

    # CSRF token required for forgery protection
    input = $('<input/>')
      .attr('type', 'hidden')
      .attr('name', 'authenticity_token')
      .attr('value', $.csrfToken())
    form.append input

    # tell rails to treat request as DELETE
    input = $('<input/>')
      .attr('type', 'hidden')
      .attr('name', '_method')
      .attr('value', 'delete')
    form.append input

    # array input for all folder id values
    for id in folders
      input = $('<input/>')
        .attr('type', 'hidden')
        .attr('name', 'folders[]')
        .attr('value', id)
      form.append input

    # array input for all file id values
    for id in files
      input = $('<input/>')
        .attr('type', 'hidden')
        .attr('name', 'files[]')
        .attr('value', id)
      form.append input

    $('body').append(form)
    form.submit()

  update: (row) ->
    # console.log row
    if row.status is 'done'
      @checkboxes.push @createCheckbox row.$('.checkbox')
    @resizeSly()

  selectedFolders: ->
    _.pluck _.select(@checkboxes, ((cb) -> cb.$el.hasClass('folder') and cb.checked)), 'value'

  selectedFiles: ->
    _.pluck _.select(@checkboxes, ((cb) -> cb.$el.hasClass('file')   and cb.checked)), 'value'

  _move: ->
    folders = @selectedFolders()
    files   = @selectedFiles()
    return unless files.length > 0 or folders.length > 0

    new chocolate.drive.views.MoveDialog
      paths:   @options.paths,
      folders: folders,
      files:   files
      id:      @id

  _delete: (e) ->
    e.preventDefault()

    folders = @selectedFolders()
    files   = @selectedFiles()
    # console.log folders, files
    return unless files.length > 0 or folders.length > 0

    @_deleteWarning @multiDelete, folders, files


  _fileUpload: (uploader, file) ->
    view = new chocolate.drive.views.FileUploadRow uploader: uploader, file: file
    @uploads.push view
    @$content.append view.render().el
    @listenTo view, 'render', @update

    # scroll to bottom
    @resizeSly()
    @sly.toEnd()
