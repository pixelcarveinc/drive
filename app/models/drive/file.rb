module Drive
  class File < ActiveRecord::Base
    IMAGE_TYPES = %w(application/pdf application/x-pdf application/postscript image/bmp image/gif image/jp2 image/jpeg image/pjpeg image/png image/tiff image/x-tga)
    
    include ::PublicActivity::Model

    private
      
      # paperclip path must be put under rails root for secure files
      def self.paperclip_path
        if Rails.application.config.drive.secure_files
          ':rails_root:url'
        else
          Paperclip::Attachment.default_options[:path]
        end
      end
      
      
    public
    
    tracked params: {title: :upload_file_name}, owner: proc { |c, m| c.current_user if c }
    
    # TODO title should not allow characters that can't be used on Windows/Linux/MacOS file systems (ie. ? > < |)
    
    # saved file resides outside of public in order for authorization to work
    has_attached_file :upload,
      path:   paperclip_path,
      styles: {
        tiny:   ['40x40#',     :jpg],
        small:  ['100x>',      :jpg],
        medium: ['250x250>',   :jpg],
        large:  ['500x500>',   :jpg],
        huge:   ['1000x1000>', :jpg] },
      convert_options: {
        tiny:   '-colorspace RGB -flatten -strip -depth 8 -quality 90',
        small:  '-colorspace RGB -flatten -strip -depth 8 -quality 90',
        medium: '-colorspace RGB -flatten -strip -depth 8 -quality 90',
        large:  '-colorspace RGB -flatten -strip -depth 8 -quality 90',
        huge:   '-colorspace RGB -flatten -strip -depth 8 -quality 90' }
        
    before_post_process :only_process_images
    after_post_process  :save_image_dimensions
    
    belongs_to :folder, inverse_of: :files
    
    validates :upload, attachment_presence: true
    # Drive accepts any type of file, explicitly tell Paperclip to allow this
    do_not_validate_attachment_file_type :upload
    # default title to file name less extension
    before_save :set_title
    before_save :set_ext
    
    
    # get title with extension
    def filename
      return nil if title.blank? || upload_file_name.blank?
      title + ::File.extname(upload_file_name)
    end
  
    def to_param
      "#{id}-#{title.create_id 64}"
    end
    
    # Clean orphaned files (no folder_id and not assigned as folder thumb).
    # Returns array of destroyed orphans.
    def self.clean
      where(folder_id: nil).select do |f|
        if Folder.where(thumbnail_id: f).count == 0
          f.destroy
        else
          false
        end
      end
    end 
    
    private  
    
      def set_title
        if title.blank?
          self.title = ::File.basename upload_file_name, ::File.extname(upload_file_name)
        end
      end
      
      def set_ext
        self.ext = ::File.extname(upload_file_name)[1..-1]
      end
      
      def only_process_images
        IMAGE_TYPES.include? upload_content_type
      end
      
      def save_image_dimensions
        # KLUDGE can't access mime type at this point? just handle the resulting non-image exception
        begin
          geo = Paperclip::Geometry.from_file upload.queued_for_write[:original]
          self.image_width  = geo.width
          self.image_height = geo.height
        rescue Paperclip::Errors::NotIdentifiedByImageMagickError
          self.image_width = self.image_height = nil
        end
      end
      
  end
end
