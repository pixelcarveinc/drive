module Drive
  class FolderDecorator < Draper::Decorator
    delegate_all
    
    decorates_association :children
    decorates_association :ancestors
    decorates_association :parent
    decorates_association :thumbnail
    decorates_association :files
    
    # path helpers
    
    def path(options = nil)
      h.folder_path object, options
    end
  
    def edit_path(options = nil)
      h.edit_folder_path object, options
    end
    
    def select_path(options = nil)
      h.select_folder_path object, options
    end
    
    def thumbnail_path(size = nil)
      unless self.thumbnail.nil? || self.thumbnail.new_record?
        self.thumbnail.download_path(size)
      end
    end
  
    # attribute helpers
  
    def alt
      object.alt || "Open #{object.name}"
    end
    
    def created_at
      object.created_at.to_s :adaptive
    end
  
    # bool helpers
  
    def fixed?
      object.fixed
    end
  
    def empty?
      !object.has_children? && object.files.empty?
    end
  
    # tag helpers
  
    def thumbnail_tag
      if object.thumbnail
        
        style = "background-image: url(#{self.thumbnail_path(:tiny)})"
        h.content_tag :span, '', class: 'thumb', style: style 
      else
        h.content_tag :span, '', class: 'drive-folder-icon'
      end
    end
  
    def link_to(options={})
      path  = options[:path]  || self.path
      title = options[:title] || self.alt
      h.link_to object.name, path, title: alt
    end
    
    def link_to_select(params = {})
      link_to path: self.select_path(params)
    end

    def link_to_sort(name, sort_column, sort_order, options = {})
      column = name.to_s.underscore.to_sym
      title  = options[:title] || name.to_s.humanize
      sorted_asc  = column == sort_column.to_sym && sort_order.to_sym == :asc
      sorted_desc = column == sort_column.to_sym && sort_order.to_sym == :desc
      
      path = self.path sort_column: column, sort_order: (sorted_asc ? :desc : :asc)
      
      h.link_to path, title: "Sort by #{title}" do
        h.concat(title)
        h.concat(h.content_tag(:span, '▲', class: 'asc'))  if sorted_asc
        h.concat(h.content_tag(:span, '▼', class: 'desc')) if sorted_desc
      end
    end
    
    def link_to_delete
      h.link_to 'Delete', self.path, { class: 'delete', title: "Delete #{object.name}",
        method: :delete, data: {confirm: "Are you sure you wish to delete #{object.name}?<br/>It cannot be recovered."}
      }
    end
  
  end
end