# Configure Rails Environment
ENV["RAILS_ENV"] = "test"

require File.expand_path("../dummy/config/environment.rb",  __FILE__)
require "rails/test_help"

Rails.backtrace_cleaner.remove_silencers!

# Load support files
Dir["#{File.dirname(__FILE__)}/support/**/*.rb"].each { |f| require f }


# Load fixtures from the engine
ActiveSupport::TestCase.fixture_path = File.expand_path("../fixtures", __FILE__)
ActionDispatch::IntegrationTest.fixture_path = File.expand_path("../fixtures", __FILE__)


class ActiveSupport::TestCase
  fixtures :all
  
  set_fixture_class 'drive/file'   => Drive::File
  set_fixture_class 'drive/folder' => Drive::Folder
end

module Drive
  class ActionController::TestCase
    setup do
      @routes = Engine.routes
    end
  end
end

require 'capybara/rails'
require 'integration/site_dsl'
require 'database_cleaner'

# set some defaults
# Capybara.current_driver = Capybara.javascript_driver # :selenium by default
# Capybara.default_wait_time = 2
DatabaseCleaner.strategy = :deletion

class ActionDispatch::IntegrationTest
  fixtures :all
  
  # Make the Capybara DSL available in all integration tests
  include Capybara::DSL
  # custom DSL for testing site
  include SiteDsl
  
  # use database cleaner instead of transactional fixtures
  self.use_transactional_fixtures = false
  
  setup do
    DatabaseCleaner.start
  end
  
  teardown do
    DatabaseCleaner.clean
  end
end