
module Drive
  class Engine < ::Rails::Engine
    isolate_namespace Drive
    
    initializer "drive.assets.precompile" do |app|
      app.config.assets.precompile += %w(drive/application.css drive/select.css drive/application.js)
    end
    
    initializer "drive.custom_time_formats" do
      Time::DATE_FORMATS[:adaptive] = lambda do |time|
        if time.today?
          format = '%l:%M %P'
        elsif time.year == Time.now.year
          format = '%b %-d'
        else
          format = '%Y'
        end
        time.localtime.strftime format
      end
    end
    
    # make some configuration options available through Rails.application.config
    config.drive = ActiveSupport::OrderedOptions.new
    config.drive.secure_files = false
  end
end
