require 'test_helper'

module Drive
  class FoldersControllerTest < ActionController::TestCase
    include Devise::TestHelpers
    
    setup do
      @folder = drive_folders :images_old
      sign_in users(:debbie)
    end
  
    test "should get index" do
      get :index, use_route: :folders
      assert_redirected_to drive_folders(:drive)
    end

    test "should get new" do
      get :new, use_route: :folders, parent_id: @folder.id
      assert_response :success
    end
  
    test "should create folder" do
      assert_difference('Folder.count') do
        post :create, use_route: :folders, folder: { parent_id: @folder.parent_id, name: @folder.name }
      end

      assert_redirected_to folder_path(assigns(:folder))
    end
  
    test "should show folder" do
      get :show, use_route: :folders, id: @folder
      assert_response :success
      assert assigns(:sort_column)
      assert assigns(:sort_order)
      assert assigns(:folders)
      assert assigns(:files)
    end

    test "should get edit" do
      get :edit, use_route: :folders, id: @folder
      assert_response :success
    end
    
    test "should show folder as json" do      
      @folder = drive_folders :drive
      get :show, format: :json, use_route: :folders, id: @folder
      assert_response :success
      drive = ActiveSupport::JSON.decode(@response.body)
      assert_equal 'Drive', drive['name']
      assert_equal 2, drive['children'].length
      assert_equal 2, drive['files'].length
    end
  
    test "should assign selected folder for show" do
      @folder = drive_folders :images_new
      get :show, use_route: :folders, id: @folder, url: 'test2.png'
      assert_response :success
      assert_not_nil assigns(:selected)
    end
  
    test "should update folder" do
      patch :update, use_route: :folders, id: @folder, folder: { parent_id: @folder.parent_id, name: @folder.name }
      assert_redirected_to folder_path(@folder.parent)
    end
  
    test "should destroy folder" do
      assert_difference('Folder.count', -1) do
        delete :destroy, use_route: :folders, id: @folder
      end

      assert_redirected_to folder_url(@folder.parent)
    end
    
    test "should multi destroy folders and files" do
      # add a folder to target folder for destroy
      @folder = drive_folders :images_new
      test = @folder.children.create! name: 'Test'
      
      assert_difference('Folder.count', -1) do
        assert_difference('File.count', -2) do
          delete :multi_destroy, use_route: :folders, folders: [test.id], files: @folder.file_ids
        end
      end
      assert_redirected_to folder_url(@folder)
    end

    
    # associated thumbnail
    
    test "should update folder with existing image" do
      patch :update, use_route: :folders, id: @folder, folder: {
        name: @folder.name, thumbnail_attributes: {id: @folder.thumbnail.id} }
      assert_redirected_to @folder.parent
    end
    
    test "should allow destroying associated thumb" do
      patch :update, use_route: :folders, id: @folder, folder: {
        thumbnail_attributes: {id: @folder.thumbnail.id, _destroy: true} }
      refute @folder.reload.thumbnail
    end
    
    
    # select files & folders
    
    test "should select destination" do
      @folder = drive_folders :drive
      get :select, use_route: :folders, id: @folder, mode: 'destination'
      
      assert_response :success
      assert_select '.pseudo-checkbox', false
      assert_select 'tr.folder', true
      assert_select 'tr.file', false
    end
    
    test "should select folders" do
      @folder = drive_folders :drive
      get :select, use_route: :folders, id: @folder, mode: 'folders', multi: 'true'
      
      assert_response :success
      assert_select 'th.select .pseudo-checkbox', true
      assert_select 'tr.folder td.select .pseudo-checkbox', true
      assert_select 'tr.file', false
    end
    
    test "should select files" do
      @folder = drive_folders :drive
      get :select, use_route: :folders, id: @folder, mode: 'files'
      
      assert_response :success
      assert_select 'th.select .pseudo-checkbox', false
      assert_select 'tr.folder td.select .pseudo-checkbox', false
      assert_select 'tr.file', true
    end
    
    test "should select both" do
      @folder = drive_folders :drive
      get :select, use_route: :folders, id: @folder, multi: 'true'
      
      assert_response :success
      assert_select 'th.select .pseudo-checkbox', true
      assert_select 'tr.folder td.select .pseudo-checkbox', true
      assert_select 'tr.folder td.select .pseudo-checkbox', true
    end
    
    test "should not select excluded" do
      @folder   = drive_folders :drive
      ex_file   = @folder.files[0]
      ex_folder = @folder.children[0]
      
      get :select, use_route: :folders, id: @folder, exclude: {files: [ex_file.id], folders: [ex_folder.id] }
      
      assert_response :success
      assert_select "tr.folder td.select input[value=#{ex_folder.id}]", false
      assert_select "tr.file td.select input[value=#{ex_file.id}]", false
      
    end
  
    # TODO rework for select action if CKEditor can use this
    # test "should redirect to folder if url param is a valid file" do
      # get :index, use_route: :folders, url: 'test2.png'
      # assert_redirected_to folder_path(drive_folders(:images_new), url: 'test2.png')
    # end
    
    
    # move files & folders
    
    test "should move files and folders" do
      drive     = drive_folders :drive
      @folder   = drive.children[1]
      mv_folder = drive.children[0]
      mv_file   = drive.files[0]
      
      post :move, use_route: :folders, id: @folder, files: [mv_file.id], folders: [mv_folder.id]
      
      assert_redirected_to folder_path(drive)
      assert_equal @folder, mv_folder.reload.parent
      assert_equal @folder, mv_file.reload.folder
    end
    
    test "should raise error if moving nothing" do
      assert_raises ArgumentError do
        post :move, use_route: :folders, id: @folder
      end
    end
    
    
    # helper methods
    
    private
      def upload_image
        fixture_file_upload '/drive/test1.jpg'
      end
  end
end
