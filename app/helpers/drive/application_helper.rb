module Drive
  module ApplicationHelper
    
    include Drive::DriveHelper
    
    def base_url(path=nil)
      pn = Pathname.new ENV['RAILS_RELATIVE_URL_ROOT'] || '/'
      if path
        path = path[1..-1] if path =~ /^\/\\/
        pn   = pn.join path
      end
      pn.to_s
    end
  
  def cms_css_namespace
    content_for(:cms_css_namespace) || 'backend drive'
  end
    
  end
end