#= require drive/namespace
#= require chocolate/style/views/forms/elements/PseudoCheckbox
#= require pxcv/views/dialogs/Dialog

PseudoCheckbox = chocolate.style.views.forms.elements.PseudoCheckbox


class chocolate.drive.views.MoveDialog extends pxcv.views.dialogs.Dialog
  
  className: 'select move dialog'
  
  events:
    'click a': '_navigate'
  
  dialog:
    width:  500
    height: 500
    title:  'Select destination folder for move'
    buttons:
      "Move Here": 'select'
      Cancel: 'close'
    
  initialize: ->
    @id      = @options.id
    @urlBase = @options.paths.folders
      
    super

  render: ->
    o = mode: 'destination', exclude: {folders: @options.folders}
    $.get "#{@urlBase}/#{@id}/select", o, (data) =>
      @$el.html data
    
    super
    
  _navigate: (e) ->
    e.preventDefault()
    
    url = $(e.currentTarget).attr('href')
    re  = new RegExp "#{@urlBase}/(\\d+)"
    @id = re.exec(url)[1]
    @render()
    
  select: ->
    # dynamically build a form for submitting items to move to target
    # could be done using Backbone.Model, but this method will utilize
    # turbolinks and rails for UI rendering
    form = $('<form></form>')
      .attr('method', 'post')
      .attr('action', @options.paths.folders + "/#{@id}/move")
      
    # CSRF token required for forgery protection
    input = $('<input/>')
      .attr('type', 'hidden')
      .attr('name', 'authenticity_token')
      .attr('value', $.csrfToken())
    form.append input
      
    # array input for all folder id values
    for id in @options.folders
      input = $('<input/>')
        .attr('type', 'hidden')
        .attr('name', 'folders[]')
        .attr('value', id)
      form.append input
        
    # array input for all file id values
    for id in @options.files
      input = $('<input/>')
        .attr('type', 'hidden')
        .attr('name', 'files[]')
        .attr('value', id)
      form.append input
      
    $('body').append(form)
    form.submit()
