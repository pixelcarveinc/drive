class AddImageDimensionColumnsToDriveFiles < ActiveRecord::Migration
  
  def change
    add_column :drive_files, :image_width,  :integer
    add_column :drive_files, :image_height, :integer
    
    Drive::File.all.each {|f| f.upload.reprocess! }
  end
end
