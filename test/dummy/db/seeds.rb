Drive::Engine.load_seed

# clean paperclip attachments
system_dir = Rails.root.join('public', 'system')
FileUtils.rm_r(system_dir) if system_dir.exist?
system_dir = Rails.root.join('system')
FileUtils.rm_r(system_dir) if system_dir.exist?
  
seed_path = Rails.root.join 'db', 'seeds'

drive = Drive::Folder.find 1
drive.files.create! upload: File.new(seed_path.join('lorem ipsum.txt'))
drive.files.create! upload: File.new(seed_path.join('Humpback Whale.jpg'))
drive.files.create! upload: File.new(seed_path.join('vlcsnap-800362.png'))

apps = drive.children.create! name: 'Apps'

pdfs = drive.children.create! name: 'PDFs', fixed: true
pdfs.files.create! upload: File.new(seed_path.join('mod_rewrite_cheat_sheet.pdf'))

images = drive.children.create! name: 'Images'
images.files.create! upload: File.new(seed_path.join('Tulips.jpg'))

images.children.create! name: 'New'

old = images.children.create! name: 'Old'
old.files.create! upload: File.new(seed_path.join('Desert.jpg'))

projects = drive.children.create! name: 'Projects'

foglers = projects.children.create! name: 'Foglers'
 
cms = foglers.children.create! name: 'CMS'
cms.files.create! upload: File.new(seed_path.join('countries.txt'))

User.create! email: 'test@example.net', password: 'password', password_confirmation: 'password', role: 'user'
manager = User.create! email: 'test2@example.net', password: 'password', password_confirmation: 'password', role: 'manager'

# set all activities owner to manager
PublicActivity::Activity.all.each { |a| a.update! owner: manager }