module Drive
  class Folder < ActiveRecord::Base
    include ::PublicActivity::Model
    
    tracked params: {title: :name}, owner: proc { |c, m| c.current_user if c }
    
    has_ancestry cache_depth: true
    has_many :files, inverse_of: :folder
    # this seems awkward that the belongs_to part of the thumbnail association
    # goes here, but it is correct, the fk is defined in this model's table
    belongs_to :thumbnail, class_name: 'File', dependent: :destroy
    accepts_nested_attributes_for :thumbnail, allow_destroy: true
    # name is required
    validates :name, :presence => true
    # prevent modification of fixed folders
    validate :can_not_modify_fixed_folder
    # prevent deletion of folder if it contains sub-folders or files
    before_destroy { raise ActiveRecord::ActiveRecordError.new('Folder must be empty') unless empty? }
    # prevent deletion of fixed folder
    before_destroy { raise ActiveRecord::ActiveRecordError.new('Folder is fixed') if fixed }
    
    
    # returns true if folder contains no files or sub-folders
    def empty?
      files.empty? and is_childless?
    end
  
    def to_param
      "#{id}-#{name.create_id 64}"
    end
    
    private
      
      def can_not_modify_fixed_folder
        if fixed && !new_record?
          errors.add :fixed, "can't modify a fixed folder"
        end
      end
      
  end
end
