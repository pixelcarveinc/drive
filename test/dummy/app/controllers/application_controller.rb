class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  
  # return the base url with ending slash or return passed url with base url prepended
  def base_url(url=nil)
    base_url = APP_CONFIG[:base_url]
    base_url = base_url.chop if base_url.end_with?('/')
    
    url = url.nil? ? '' : url.to_s
    url.slice!(0) if url.start_with?('/')
    base_url + '/' + url
  end
  
  
  private
  
    def authenticate!
      authenticate_user!
    end
end
