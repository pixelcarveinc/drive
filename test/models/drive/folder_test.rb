require 'test_helper'

module Drive
  class FolderTest < ActiveSupport::TestCase
    
    setup do
      @folder = drive_folders :images
    end
    
    test "only destroy if empty" do
      assert_nothing_raised do
        drive_folders(:images_old).destroy
      end
      # should not work, has sub-folders
      assert_raise(ActiveRecord::ActiveRecordError) do
        drive_folders(:images).destroy
      end
      # should not work, has files
      assert_raise(ActiveRecord::ActiveRecordError) do
        drive_folders(:images_new).destroy
      end
    end
    
    test "empty?" do
      # empty
      assert_equal true, drive_folders(:images_old).empty?
      # has sub-folders
      assert_equal false, drive_folders(:images).empty?
      # has files
      assert_equal false, drive_folders(:images_new).empty?
    end
    
    test "name is required" do
      new_folder = Folder.new parent_id: @folder.parent_id
      assert_equal false, new_folder.save
    end
    
    test "has ancestry" do
      images_new = drive_folders :images_new
      assert_equal @folder, images_new.parent
    end
    
    test "has files" do
      images_new = drive_folders :images_new
      assert_equal true, images_new.files.count > 0
    end
    
    test "should not allow update for fixed folder" do
      drive = drive_folders :drive
      drive.name = 'invalid'
      assert !drive.save
    end
    
    test "should not allow delete for fixed folder" do
      assert_raise(ActiveRecord::ActiveRecordError) do
        drive_folders(:pdfs).destroy
      end
    end
    
    test "should allow new fixed folder" do
      folder = Folder.new name: 'Test', fixed: true
      folder.parent = @folder
      assert folder.save
    end
    
    
    # thumbnail tests
    
    test "should be able to access thumbnail" do
      assert_not_nil drive_folders(:images_old).thumbnail
    end
    
    test "should cascade destroy thumbnail" do
      assert_difference('File.count', -1) do
        drive_folders(:images_old).destroy
      end
    end
  
    test "should include slug in to_param" do
      assert_equal "#{@folder.id}-#{@folder.name.create_id 64}", @folder.to_param
    end
    
  end
end
