module Drive
  class FileDecorator < Draper::Decorator
    delegate_all
    
    decorates_association :folder
    
    # path helpers
    
    def path
      if object.folder
        h.folder_file_path object.folder, object
      else
        h.file_path object.id
      end
    end
    
    def edit_path
      if object.folder
        h.edit_folder_file_path object.folder, object
      else
        h.edit_file_path object
      end
    end
    
    def download_path(size = nil)
      if Rails.application.config.drive.secure_files
        if object.folder
          h.download_folder_file_path object.folder, object, size: size
        else
          h.download_file_path object, size: size
        end
      else
        object.upload.url(size)
      end
    end
    
    # attribute helpers
  
    def alt
      if object.alt.blank?
        "Open #{object.filename}"
      else
        object.alt
      end
    end
    
    def created_at
      object.created_at.to_s :adaptive
    end
    
    def upload_file_size
      h.number_to_human_size object.upload_file_size
    end
    
    def ext
      object.ext.blank? ? 'default' : object.ext.downcase
    end

    # bool helpers

    def image?
      File::IMAGE_TYPES.include? object.upload_content_type
    end
  
    # tag helpers
  
    def thumbnail_tag
      if self.image?
        self.lightbox_tag do
          style = "background-image: url(#{download_path(:tiny)})"
          h.content_tag :span, '', class: 'thumb', style: style
        end
      else
        self.type_image_tag
      end
    end
      
    def lightbox_tag(content = nil)
      h.link_to download_path, title: self.alt, data: { lightbox: "file" } do
        content || yield
      end
    end
  
    def type_image_tag
      h.content_tag :span, '', class: "drive-file-icon drive-file-icons-#{self.ext}"
    end
    
    def type_image_url
      if ::File.exist? Rails.root.join('public', 'drive', 'file-icons', "#{self.ext}.png")
        image_path "drive/file-icons/#{self.ext}.png"
      else
        image_path "drive/file-icons/default.png"
      end
    end
    
    def link_to_download
      h.link_to object.filename, self.download_path, target: '_blank', title: object.alt
    end
    
    def link_to_delete
      h.link_to 'Delete', self.path, { class: 'delete', title: "Delete #{object.filename}",
        method: :delete, data: {confirm: "Are you sure you wish to delete #{object.filename}?<br/>It cannot be recovered."}
      }
    end
  
  end
end