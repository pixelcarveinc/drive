Drive::Engine.routes.draw do

  resources :folders do
    resources :files, except: [:index, :new] do
      # secure file access
      get 'download', on: :member

    end



    # destroy multiple files and folders
    delete :index, on: :collection, action: :multi_destroy

    # context aware search in folder
    get 'search', on: :member

    # select something from folder contents
    get 'select', on: :member
    get 'select', on: :collection

    # move files and folders to folder
    post 'move', on: :member


    delete ':id', action: :destroy, controller: :files
    get ':id', action: :show, controller: :files
    put ':id', action: :update, controller: :files
  end

  # routes for modifying and downloading folder thumbnails
  #
  # used to use an AJAX constraint for these but jQuery File Upload uses iframe
  # transport which uses a non-XHR post
  resources :files, only: [:create, :update, :destroy] do
    # secure file access
    get 'download', on: :member
  end
end
