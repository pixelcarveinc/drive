require 'test_helper'

class AbilityTest < ActiveSupport::TestCase

  setup do
    @folder = drive_folders :drive
  end
  
  test "read only auth" do
    user_ability :ted
    assert @ability.can? :read, @folder
    assert @ability.can? :read, @folder.files[0]
    assert @ability.cannot? :create, Drive::Folder
    assert @ability.cannot? :create, Drive::File
    assert @ability.cannot? :update, @folder
    assert @ability.cannot? :update, @folder.files[0]
    assert @ability.cannot? :destroy, @folder
    assert @ability.cannot? :destroy, @folder.files[0]
  end
  
  
  private
    def user_ability(user)
      @user    = users user
      @ability = Ability.new @user
    end
    
end