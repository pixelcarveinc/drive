# encoding: UTF-8

class String

  # Translate special latin characters to their (roughly) equivalent English
  # characters.
  def convert_latin
    translate = {
      'À' => 'A',    'Á' => 'A',     'Â' => 'A',     'Ã' => 'A',
      'Ä' => 'AE',   'Å' => 'A',     'Æ' => 'AE',    'Ç' => 'C',
      'È' => 'E',    'É' => 'E',     'Ê' => 'E',     'Ë' => 'E',
      'Ì' => 'I',    'Í' => 'I',     'Î' => 'I',     'Ï' => 'I',
      'Ð' => 'D',    'Ñ' => 'N',     'Ò' => 'O',     'Ó' => 'O',
      'Ô' => 'O',    'Õ' => 'O',     'Ö' => 'OE',    'Ø' => 'O',
      'Ù' => 'U',    'Ú' => 'U',     'Û' => 'U',     'Ü' => 'UE',
      'Ý' => 'Y',    'Þ' => 'Th',    'ß' => 'ss',    'à' => 'a',
      'á' => 'a',    'â' => 'a',     'ã' => 'a',     'ä' => 'ae',
      'å' => 'a',    'æ' => 'ae',    'ç' => 'c',     'è' => 'e',
      'é' => 'e',    'ê' => 'e',     'ë' => 'e',     'ì' => 'i',
      'í' => 'i',    'î' => 'i',     'ï' => 'i',     'ð' => 'd',
      'ñ' => 'n',    'ò' => 'o',     'ó' => 'o',     'ô' => 'o',
      'õ' => 'o',    'ö' => 'oe',    'ø' => 'o',     'ù' => 'u',
      'ú' => 'u',    'û' => 'u',     'ü' => 'ue',    'ý' => 'y',
      'þ' => 'th',   'ÿ' => 'y',     'Œ' => 'Oe',    'œ' => 'oe',
      'Š' => 'S',    'š' => 's',     'Ÿ' => 'Y'
    }
    
    s = self.to_s
    translate.each_pair {|k, v| s.gsub! k, v }
    s
  end

  def convert_latin!
    self.replace self.convert_latin
  end
  
  
  # Converts a string to an ID containing only "a-z0-9\_\-".  Special latin
  # characters are converted to their a-z equivalents.
  def create_id(limit=nil)

    short_forms = {
      '%'  => '-percent-',
      '$'  => '-dollars-',
      '='  => '-equals-',
      '°'  => '-degrees-',
      '©'  => '-copyright-',
      '@'  => '-at-',
      '+'  => '-and-',
      '&'  => '-and-',
      '–'  => '-',
      '—'  => '-',
      '.'  => '-',
      ' '  => '-'
    }
  
    # clean up and convert special latin characters
    id = self.strip.convert_latin.downcase
    
    # replace common short forms
    short_forms.each_pair {|k, v| id.gsub! k, v }
    
    # remove anything not alpha numeric or dash/underscore
    id.gsub!(/[^a-z0-9\_\-]/, '')
    # remove duplicate dashes
    id.gsub!(/-{2,}/, '-')
    # remove dashes from start/end
    id.gsub!(/^[\-_]+|[\-_]+$/, '')
    
    # limit if supplied
    if !limit.nil? && id.length > limit
      parts = id.split '-'
      id = ''
      parts.each do |part|
        break if (id + part).length > limit
        id += part + '-'
      end
      id = parts[0] if id == '' && parts.length
      id = id[0...-1] if id[-1] == '-'
    end
    
    id
  end
  
  def create_id!(limit=nil)
    self.replace self.create_id(limit)
  end
end