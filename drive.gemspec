$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "drive/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "drive"
  s.version     = Drive::VERSION
  s.authors     = ["Pixelcarve Inc."]
  s.email       = ["operations@pixelcarve.com"]
  s.homepage    = "http://pixelcarve.com/"
  s.summary     = "Drive containing folders and files"
  s.description = "Drive containing folders and files"

  s.files = Dir["{app,config,db,lib,vendor}/**/*"] + ["Rakefile", "README.rdoc"]
  s.test_files = Dir["test/**/*"]

  s.add_dependency "rails", "~> 4.0"
  s.add_dependency "ancestry"
  s.add_dependency "paperclip"
  s.add_dependency "cancan"
  s.add_dependency "thinking-sphinx"
  s.add_dependency 'public_activity'
  s.add_dependency "haml-rails"
  s.add_dependency "draper"
  s.add_dependency "turbolinks"
  s.add_dependency "uglifier", '>= 1.3'
  s.add_dependency "coffee-rails", '~> 4.0'
  s.add_dependency "haml_coffee_assets"
  s.add_dependency "sass-rails", '~> 5.0.1'
  s.add_dependency "compass-rails"
  s.add_dependency 'pxcv-rails'
  s.add_dependency 'chocolate-style'

  s.add_development_dependency "sqlite3"
  s.add_development_dependency "mysql2"
  s.add_development_dependency "devise"
  s.add_development_dependency "capybara"
  s.add_development_dependency "database_cleaner"
  s.add_development_dependency "selenium-webdriver"
end
