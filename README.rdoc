= Drive


TESTING WITHIN A LOCAL PROJECT

gem "drive", :git => "pcnet@pixelcarve.net:~/git/drive.git"


BUILDING

gem build drive.gemspec


INSTALL

gem install drive-[VERSION].gem

Copy spec, cache and gem to project vendor/ruby/[VERSION]/ directories


POST INSTALL

rails c
Drive::Engine.load_seeds


DEVELOPMENT/TESTING

Run dummy application by running 'rails s' in test/dummy

test users:
test@example.net	- limited access
test2@example.net	- full access

password for both is 'password'

log out URL: /users/log_out
