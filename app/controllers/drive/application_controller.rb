module Drive
  class ApplicationController < ::ApplicationController
    include ::PublicActivity::StoreController
    
    check_authorization
    
    before_action :authenticate!
    layout :pick_layout
    
    
    private
    
      def set_folder_breadcrumbs(folder = nil)
        folder = @folder unless folder
        return if folder.nil? || folder.new_record?
        
        folder.ancestors.each do |f|
          add_breadcrumb f.name, f
        end
        add_breadcrumb folder.name, folder
      end
      
      def pick_layout
        # no layout for XHR requests in case of HTML data type
        false if request.xhr?
      end
      
  end
end
