require "drive/engine"
# requires below fix various problems when mounting the engine in an app
require "haml"
require "haml_coffee_assets"
require "compass-rails"
require "ancestry"
require "paperclip"
require "devise"
require "cancan"
require "thinking-sphinx"
require "public_activity"

module Drive
end
