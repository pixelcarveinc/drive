module Drive
  # General Drive helpers in a separate module so they may be included in apps
  # mounting the drive
  module DriveHelper
    
    # Render templates of a different format than calling template
    # http://stackoverflow.com/questions/339130/how-do-i-render-a-partial-of-a-different-format-in-rails
    def with_format(format, &block)
      old_formats = formats
      begin
        self.formats = [format]
        return block.call
      ensure
        self.formats = old_formats
      end
    end
    
    # TODO this will probably change significantly
    # Serialize complete folder tree to JSON
    # (uses jbuilder templates instead of straight to_json)
    def tree_json(tree=nil, folder=nil)
      tree = Folder.arrange order: :name unless tree
      
      with_format(:json) do
        render file: 'drive/folders/_branch', locals: {branch: tree, full: folder}
      end
    end
    
    # Serialize a folder to JSON
    # (uses jbuilder templates instead of straight to_json)
    def folder_json(folder=nil)
      folder = @folder unless folder
      return nil.to_json unless folder
    
      with_format(:json) do
        render file: 'drive/folders/show', locals: {:@folder => folder}
      end
    end
    
    # Serialize root folders to JSON
    def roots_json
      Drive::Folder.roots.to_json
    end
    
  end
end