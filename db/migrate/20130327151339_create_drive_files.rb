class CreateDriveFiles < ActiveRecord::Migration
  def change
    create_table :drive_files do |t|
      t.integer :folder_id
      t.timestamps
    end
    
    add_attachment :drive_files, :upload
    add_index :drive_files, :folder_id, :name => 'idx_drive_files_folder_id'
  end
end
