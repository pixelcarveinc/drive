module SiteDsl
  
  private
  
  
  # finders
  
  # Get a row by file or folder name.
  def find_row(title)
    find('td.title a', text: title).find(:xpath, '..').find(:xpath, '..')
  end

  def rows_selected
    all('.content .select input').select {|e| e.checked? }.count
  end
  
  # Within dialog on page.  Optional index for when more than one dialog is open.
  def within_dialog(index=0)
    within(all('.ui-dialog')[index]) { yield }
  end


  # navigation helpers

  # Sign in a user, signs out existing if it's not the same.
  def sign_in(email='test2@example.net')
    visit '/users/sign_in'
    unless page.has_content? "Logged in as #{email}"
      if page.has_content? 'Logged in'
        visit '/users/sign_out'
        visit '/users/sign_in'
      end
      
      within '#new_user' do
        fill_in 'Email', with: email
        fill_in 'Password', with: 'password'
      end
      click_button 'Sign in'
      
      assert page.has_content?("Logged in as #{email}"), 'could not log in'
    end
  end


  # assertions
  
  def assert_navigation(*args)
    find '#header h2', text: args[0]
    assert_breadcrumb args.reverse.map {|a| a.upcase }
  end
  
  def assert_breadcrumb(parts)
    els = page.all('#breadcrumbs > *')
    els.each_with_index do |el, i|
      case i
      when els.length
        el.find '.last', text: parts[i]
      else
        el.find 'a', text: parts[i]
      end
    end
  end
  
  def assert_rows(*args)
    args.each_with_index do |title, i|
      find ".content tr:nth-child(#{i + 1})", text: title
    end
  end
  
  def assert_sort(column, direction)
    page.all('#listing .headings a').each do |button|
      directions = {asc: false, desc: false}
      if button['data-col'] == column
        directions[:asc]  = true if direction == 'asc'
        directions[:desc] = true if direction == 'desc'
      end
      directions.each_pair do |dir, vis|
        button.find '.' + dir.to_s, visible: vis
      end  
    end
  end
  
  
  # misc
  
  # Fill in a CKEditor textarea.
  def fill_in_ck(id, options = {})
    # make sure CKEDITOR has been instantiated
    find "#cke_#{id}"
    page.execute_script "CKEDITOR.instances['#{id}'].insertText('#{options[:with]}')"
  end
  
  # Attach a file for a JS controlled file upload.
  def attach_file_js(id, *files)
    files.each do |f|
      # TODO Windows specific fix, should check platform before replacing slashes
      file = Drive::Engine.root.join(f).to_s.gsub(/\//, '\\')
      attach_file id, file, visible: false
    end
  end
end