require 'test_helper'

module Drive
  class FilesControllerTest < ActionController::TestCase
    include Devise::TestHelpers
    
    setup do
      @file   = drive_files :one
      @folder = drive_folders :images_new
      sign_in users(:debbie)
    end

  
    test "should create file" do
      upload = fixture_file_upload '/drive/test1.jpg'
      assert_difference('File.count') do
        post :create, use_route: :files, folder_id: @folder.id, file: {upload: upload}
      end

      assert_redirected_to edit_folder_file_path(@folder, assigns(:file))
    end

    test "should create file from multi-upload" do
      upload = fixture_file_upload '/drive/test1.jpg'
      assert_difference('File.count') do
        post :create, use_route: :files, folder_id: @folder.id, file: {upload: [upload]}
      end
      
      assert_redirected_to edit_folder_file_path(@folder, assigns(:file))
    end
  
    test "should show file" do
      get :show, format: :json, use_route: :files, folder_id: @folder.id, id: @file
      assert_response :success
      file = ActiveSupport::JSON.decode @response.body
      assert_not_nil file['upload_file_name']
      assert_not_nil file['image_width']
    end

    test "should get edit" do
      get :edit, use_route: :files, folder_id: @folder.id, id: @file
      assert_response :success
    end
  
    test "should update file" do
      upload = fixture_file_upload '/drive/test1.jpg'
      patch :update, use_route: :files, folder_id: @folder.id, id: @file, file: {upload: upload}
      assert_redirected_to @folder
    end
        
    test "should destroy file" do
      assert_difference('File.count', -1) do
        delete :destroy, use_route: :files, folder_id: @folder.id, id: @file
      end
  
      assert_redirected_to @folder
    end
    
    
    # TODO handle these with association attributes?
    
    # tests for files without folder id (folder thumbnails)
=begin
    test "should create standalone file" do
      upload = fixture_file_upload '/drive/test1.jpg'
      post :create, format: :json, use_route: :files, file: {upload: upload}
      assert_response :created
      file = ActiveSupport::JSON.decode @response.body
      assert_not_nil file['upload_file_name']
    end
    
    test "should update standlone file" do
      upload = fixture_file_upload '/drive/test1.jpg'
      patch :update, format: :json, use_route: :files, id: @file, file: {upload: upload}
      assert_response :no_content
    end
    
    test "should destroy standalone file" do
      assert_difference('File.count', -1) do
        delete :destroy, use_route: :files, id: @file
      end
      assert_response :no_content
    end
=end
  end
end
