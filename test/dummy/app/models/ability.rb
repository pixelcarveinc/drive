# test ability, it will be up to the mounting app to decide who can access the
# drive's resources
class Ability
  include CanCan::Ability

  def initialize(user)
    
    alias_action :download, :search, :to => :read
    
    if user.is? :user
      can :read, [Drive::Folder]
      can :read, [Drive::File]
    end
    
    can :manage, :all if user.is? :manager
  end
end
