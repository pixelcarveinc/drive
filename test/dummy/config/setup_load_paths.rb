# This prevents Rack version conflicts when running under Passenger
# http://stackoverflow.com/a/11909016

require 'rubygems'
require 'bundler/setup'
