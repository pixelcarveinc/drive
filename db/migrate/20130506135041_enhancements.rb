class Enhancements < ActiveRecord::Migration
  
  def change
    add_column :drive_folders, :alt, :string
    add_column :drive_folders, :body, :text
    add_column :drive_folders, :display_mode, :string, default: 'list'
    add_column :drive_folders, :thumbnail_id, :integer
    add_column :drive_folders, :parent_id, :integer
    add_column :drive_folders, :ancestry_depth, :integer, :default => 0
    add_index :drive_folders, :thumbnail_id, name: 'idx_drive_folders_thumbnail_id'
    add_index :drive_folders, :parent_id, name: 'idx_drive_folders_parent_id'
    
    add_column :drive_files, :title, :string
    add_column :drive_files, :alt, :string
    add_column :drive_files, :body, :text
    add_column :drive_files, :ext, :string
  end
end
