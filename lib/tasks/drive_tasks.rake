namespace :drive do
  desc "Destroy orphaned files"
  task clean: :environment do
    destroyed = Drive::File.clean
    puts "cleaned #{destroyed.count} orphan files"
  end
end
