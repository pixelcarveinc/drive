require_dependency "drive/application_controller"

module Drive
  class FoldersController < ApplicationController
    load_and_authorize_resource except: [:create], class: Folder

    layout :select_layout, only: :select

    before_action :set_folder_breadcrumbs, only: [:show, :new, :edit]

    decorates_assigned :folder, :folders, :files


    # redirects to show action for root folder
    # GET /drive/folders
    # GET /drive/folders.json
    def index
      redirect_to Folder.roots[0]
    end

    # show contents of a specific folder
    # GET /drive/folders/1
    # GET /drive/folders/1.json
    def show
      #byebug
      # in select mode a url may be passed indicating currently selected file
      # it will be marked selected if found
      if params[:url]
        @selected = @folder.files.find_by_upload_file_name params[:url]
      end

      @sort_column = params[:sort_column] || :title
      @sort_order  = params[:sort_order]  || :asc
      sort = "#{@sort_column} #{@sort_order}"

      # perform ordering here and pass to view as folder/files decorated
      case @sort_column.to_sym
      when :title
        @folders = @folder.children.order("name #{@sort_order}")
        @files   = @folder.files.order(sort)
      when :created_at
        @folders = @folder.children.order(sort)
        @files   = @folder.files.order(sort)
      when :upload_content_type, :upload_file_size
        @folders = @folder.children
        @files   = @folder.files.order(sort)
      end

      respond_to do |format|

        format.html { 
          #byebug
          render action: 'show'
        }
        format.json { 

          render json: @folder.as_json(include: [:files, :children, :parent]) 
        }
      end
    end

    # GET /drive/folders/new
    def new
      unless params[:parent_id].nil?
        @parent = Folder.find params[:parent_id]
        set_folder_breadcrumbs @parent
        add_breadcrumb 'New', new_folder_path(parent_id: @parent.id)

        @folder.parent = @parent
      end
    end

    # GET /drive/folders/1/edit
    def edit
      add_breadcrumb 'Edit', edit_folder_path(@folder)
    end

    # POST /drive/folders
    # POST /drive/folders.json
    def create
      @folder = Folder.new folder_params
      authorize! :create, @folder

      respond_to do |format|
        if @folder.save
          format.html { redirect_to @folder, notice: 'Folder was successfully created.' }
          format.json { render action: 'show', status: :created, location: @folder }
        else
          format.html { render action: 'new' }
          format.json { render json: @folder.errors, status: :unprocessable_entity }
        end
      end
    end

    # PATCH/PUT /drive/folders/1
    # PATCH/PUT /drive/folders/1.json
    def update
      respond_to do |format|
        if @folder.update(folder_params)
          format.html { redirect_to_folder_parent @folder, notice: "#{folder.name} was successfully updated." }
          format.json { render action: 'show' }
        else
          format.html { render action: 'edit' }
          format.json { render json: @folder.errors, status: :unprocessable_entity }
        end
      end
    end

    # DELETE /drive/folders/1
    # DELETE /drive/folders/1.json
    def destroy
      @folder.destroy

      respond_to do |format|
        format.html { redirect_to_folder_parent(@folder, url: true) }
        format.json { head :no_content }
      end
    end

    # DELETE /drive/folders
    # DELETE /drive/folders.json
    def multi_destroy
      # must be defined outside of block
      parent = nil

      Folder.transaction do
        if params[:files]
          @files = Drive::File.find params[:files]
          authorize! :destroy, @files

          parent = @files.first.folder
          Drive::File.destroy @files
        end

        if params[:folders]
          @folders = Drive::Folder.find params[:folders]
          authorize! :destroy, @folders

          parent = @folders.first.parent
          Drive::Folder.destroy @folders.find_all {|f| f.empty? && !f.fixed }
        end
      end

      respond_to do |format|
        format.html { redirect_to parent }
        format.json { head :no_content }
      end
    end

    # GET /drive/folders/select
    # GET /drive/folders/1/select
    def select

      # NOTE not currently used with CKEditor, implement if possible & rework for select
      # in select mode a url may be passed indicating currently selected file
      # redirect to containing folder if file is found
      # if params[:url]
        # file = File.where('upload_file_name = ? AND folder_id NOT NULL', params[:url]).first
        # if file
          # redirect_to folder_path(file.folder, url: params[:url])
          # return
        # end
      # end

      #byebug
      # default to root folder if not specified
      @folder = Folder.roots[0] unless @folder
      @select_mode = (params[:mode] || :both).to_sym
      @select_multi = params[:multi] == 'true'

      excludes = HashWithIndifferentAccess.new folders: [], files: []
      excludes.merge!(params[:exclude]) if params[:exclude]

      @folders = @folder.children.where('id NOT IN (?)', excludes[:folders].join(','))
      @files   = @folder.files.where('id NOT IN (?)', excludes[:files].join(','))

      @params = params
      @params.delete :id
    end

    # POST /drive/folders/1/move
    # POST /drive/folders/1/move.json
    def move
      @folders = params[:folders] || []
      @files   = params[:files]   || []

      @folders = Drive::Folder.find @folders
      @files   = Drive::File.find @files

      raise ArgumentError.new('nothing to move') if @folders.empty? && @files.empty?

      authorize! :update, @folders
      authorize! :update, @files

      @parent = @folders.any? ? @folders[0].parent : @files[0].folder

      Folder.transaction do
        @folders.each { |folder| folder.update! parent_id: @folder.id }
        @files.each { |file| file.update! folder_id: @folder.id }
      end

      respond_to do |format|
        format.html { redirect_to folder_url(@parent) }
        format.json { head :no_content }
      end
    end

    # Context aware search within a folder
    # GET /drive/folders/1/search.json
    def search
      per_page = params[:per_page] || 20

      options = {
        page: params[:page], per_page: per_page,
        with: {
          # context aware, only show results for this folder branch
          folder_id: @folder.subtree_ids
        }
      }

      if params[:q] == '[size]'
        @results = search_size options
      else
        @results = search_general params[:q], options
      end

      respond_to do |format|
        format.json { render json: @results }
      end
    end


    private

      # Set custom layout for select action when not an XHR request.
      def select_layout
        request.xhr? ? false : 'drive/select'
      end

      def search_general(query, options)
        options[:classes]       = [Drive::Folder, Drive::File]
        options[:field_weights] = {
            name:  10,
            alt:    5,
            body:   2
        }

        # search accross folders and files
        ThinkingSphinx.search Riddle::Query.escape(query), options
      end

      def search_size(options)
        options[:order] = 'upload_file_size DESC'

        Drive::File.search options
      end

      def folder_params
        params.require(:folder).permit(:alt, :body, :fixed, :name, :parent_id,
          :thumbnail_id, thumbnail_attributes: [:id, :folder_id, :upload, :_destroy])
      end

      # Redirect in order of availabliliting to: folder parent, folder,
      # folders index
      def redirect_to_folder_parent(folder, options = {})
        options.symbolize_keys!

        url = options.delete :url
        folder = (folder && folder.parent) || folder

        if url
          redirect_to folder ? folder_url(folder) : folders_url, options
        else
          redirect_to folder ? folder : folders_path, options
        end
      end

  end
end
