require 'test_helper'

class FileAndFolderFlowsTest < ActionDispatch::IntegrationTest

  setup do
    Capybara.current_driver = Capybara.javascript_driver
    sign_in
  end

  test "create, update and delete file" do
    visit '/drive/folders'
    
    attach_file_js 'file_upload', test_file
    
    # should complete upload
    new_file = find_row('test1.jpg')
    
    # opens lightbox
    new_file.first('.title a').click
    find('#lightbox .lb-close').click
    
    # edit file
    new_file.find('.edit').click

    find '#header h2', text: 'Edit test1.jpg'

    within 'form' do
      fill_in 'file_title', with: 'test-file'
      fill_in 'file_alt', with: 'Test alt'
      fill_in_ck 'file_body', with: 'Test body'
      click_button 'Save'
    end
    
    # should have been updated
    find_row('test-file').find('.edit').click
    
    assert_equal 'Test alt', find("#file_alt").value
    assert_equal "<p>Test body</p>\n", find("#file_body", visible: false).value

    find('#controls .back').click
    
    # delete file
    find_row('test-file').find('.delete').click

    within_dialog { click_button 'OK' }
    
    # should have been deleted
    assert page.has_no_text? 'test-file'
  end
  
  test "create, update and delete folder" do
    visit '/drive/folders'
    
    find('#controls a', text: 'New Folder').click
    
    within 'form' do
      fill_in 'folder_name', with: 'Test Folder'
      click_button 'Save'
    end
    
    # should have been created
    find '#header h2', text: 'Test Folder'
    
    # go up a level
    find('td.up a', text: 'Up one level').click
    
    # edit folder
    find_row('Test Folder').find('.edit').click

    find '#header h2', text: 'Edit Test Folder'
    
    within 'form' do
      fill_in 'folder_name', with: 'Test Folder 2'
      attach_file_js 'folder_thumbnail_attributes_upload', test_file
      fill_in_ck 'folder_body', with: 'Test body'
      click_button 'Save'
    end

    # should have been updated
    find_row('Test Folder 2').find('.edit').click
    
    find '.image-field img'
    assert_equal "<p>Test body</p>\n", find("#folder_body", visible: false).value

    find('#controls .back').click
    
    # delete folder
    find_row('Test Folder 2').find('.delete').click

    within_dialog { click_button 'OK' }
    
    # should have been deleted
    assert page.has_no_text? 'Test Folder 2'
  end

  test "move files and folders" do
    visit '/drive/folders'
    
    # select a file and a folder
    find_row('important.doc').find('button').click
    find_row('Images').find('button').click
    
    # open move dialog
    find('#controls .move').click
    
    within_dialog do
      find_row('PDFs').find('a').click
      click_button 'Move Here'
    end
    
    # should have moved
    assert find('#listing .content').has_no_text? 'important.doc'
    assert find('#listing .content').has_no_text? 'Images'
    
    find_row('PDFs').first('a').click
    
    find '#header h2', text: 'PDFs'
    
    assert find('#listing .content').has_text? 'important.doc'
    assert find('#listing .content').has_text? 'Images'
  end

  test "delete files and folders" do
    visit '/drive/folders'
    
    # select a file and a folder
    find_row('important.doc').find('button').click
    find_row('Images').find('button').click
    
    # open delete dialog
    find('#controls .delete').click
    
    within_dialog do
      click_button 'Yes'
    end
    
    # should have been deleted
    assert find('#listing .content').has_no_text? 'important.doc'
    # should not have been deleted (not empty)
    assert find('#listing .content').has_text? 'Images'
  end
  
  private
    def test_file
      'test/fixtures/drive/test1.jpg'
    end
end
