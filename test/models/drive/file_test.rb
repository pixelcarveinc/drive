require 'test_helper'

module Drive
  class FileTest < ActiveSupport::TestCase
    
    # required for fixture_file_upload
    include ActionDispatch::TestProcess
    
    setup do
      @file   = drive_files :one
      @folder = drive_folders :images_new
    end
    
    test "belongs to folder" do
      assert_equal @folder, @file.folder 
    end
    
    test "requires attached file" do
      @file = @folder.files.new
      assert !@file.save
    end
    
    test "should default title to upload file name without extension" do
      file = @folder.files.new
      file.upload = fixture_file_upload '/drive/test1.jpg'
      file.save!
      assert_equal 'test1', file.title
    end
    
    test "filename method should return title with file extension" do
      file = File.new
      assert file.filename.nil?
      file.title = 'Test'
      assert file.filename.nil?
      file.upload = fixture_file_upload '/drive/test1.jpg'
      assert_equal 'Test.jpg', file.filename
    end
    
    test 'should set ext on save' do
      file = File.new
      file.upload = fixture_file_upload '/drive/test1.jpg'
      file.save!
      assert_equal 'jpg', file.ext
    end
  
    test "should include slug in to_param" do
      assert_equal "#{@file.id}-#{@file.title.create_id 64}", @file.to_param
    end
    
    test "clean should destroy orphaned files" do
      assert_difference('File.count', -1) do
        File.clean
      end
    end
    
    test "should set image_width and image_height for images" do
      f = File.new upload: fixture_file_upload('/drive/test1.jpg')
      assert_equal 236, f.image_width
      assert_equal 355, f.image_height
    end
    
    test "should save non-image files" do
      f = File.new upload: fixture_file_upload('/drive/test2.docx')
      assert f.save
    end
  end
end
