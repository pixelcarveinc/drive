# This migration comes from drive (originally 20130322231355)
class CreateDriveFolders < ActiveRecord::Migration
  
  def up
    create_table :drive_folders do |t|
      t.string :name
      t.string :ancestry

      t.timestamps
    end
    add_index :drive_folders, :ancestry, :name => 'idx_drive_folders_ancestry'
  end
  
  def down
    remove_index :drive_folders, :name => 'idx_drive_folders_ancestry'
    drop_table :drive_folders
  end
end
