#= require drive/namespace


class window.chocolate.drive.views.Selector extends chocolate.drive.views.FolderListing
  
  events:
    'click #controls .select': '_select'
    'click #controls .cancel': '_cancel'

  initialize: ->
    super
    
    @ckeditor = opener.CKEDITOR
    
    $(window).on 'resize', _.debounce(@_resize, 100)
    @_resize()
    
    $(document).on 'page:before-change', @_beforeChange

  cleanUp: ->
    super
    
    $(window).off   'resize'
    $(document).off 'page:before-change'
  
  remove: ->
    super
    close()
  
  _select: ->
    checked = _.find(@checkboxes, ((cb) -> cb.$el.hasClass('file') and cb.checked))
    if checked?
      url = checked.$el.closest('tr.file').data('url')
      @ckeditor.tools.callFunction @options.CKEditorFuncNum, url
      @remove()
    
  _cancel: -> @remove()

  _resize: => @resize()
    
  _beforeChange: =>
    @cleanUp()
    delete window.selector
