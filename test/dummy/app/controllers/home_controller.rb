class HomeController < ApplicationController
  before_action :authenticate!, except: :index
    
  def index
  end
  
  def activity
    @activities = PublicActivity::Activity.where('owner_id = :id OR recipient_id = :id', id: current_user.id).order('created_at DESC').limit(25)
  end
  
  def pages
  end
end
