require_dependency "drive/application_controller"

module Drive
  class FilesController < ApplicationController

    before_action :set_folder
    before_action :fix_multiuploads
    after_action  :fix_iframe_header, only: [:create, :update]
    load_and_authorize_resource except: [:create], class: Drive::File

    decorates_assigned :folder, :file


    # GET /drive/folder/1/files/1.json
    def show
  
      respond_to do |format|
        format.json { render json: @file }
      end
    end

    # GET /drive/folders/1/files/1/edit
    def edit
      set_folder_breadcrumbs
      add_breadcrumb @file.filename, edit_folder_file_path(@folder, @file)
    end

    # create a new file from an upload
    # POST /drive/folder/1/files.json
    # POST /drive/files.json
    def create
      @file = File.new(file_params)
      @file.folder = @folder
      authorize! :create, @file

      respond_to do |format|
        if @file.save
          format.html do
            if request.xhr?
              # XHR gets a fragment
              render file: 'drive/folders/_file.html'
            else
              # HTML is redirected
              redirect_to edit_folder_file_path(@file.folder, @file), notice: 'File was successfully created.'
            end
          end
          format.json { render json: @file, status: :created, location: [@folder, @file] }
        else
          format.html do
            if request.xhr?
              # XHR gets an exception
              raise 'File upload failed.'
            else
              # HTML gets a redirect with notice
              redirect_to @folder, notice: 'File upload failed.'
            end
          end
          format.json { render json: @file.errors, status: :unprocessable_entity }
        end
      end
    end

    # PATCH/PUT /drive/folder/1/files/1.json
    # PATCH/PUT /drive/files/1.json
    def update
      #debugger
      respond_to do |format|
        if @file.update(file_params)
          format.html { redirect_to @folder, notice: 'File was successfully updated.' }
          format.json { head :no_content }
        else
          format.html { render action: 'edit' }
          format.json { render json: @file.errors, status: :unprocessable_entity }
        end
      end
    end

    # DELETE /drive/folder/1/files/1.json
    # DELETE /drive/files/1.json
    def destroy
      @file.destroy

      respond_to do |format|
        format.html { redirect_to @folder }
        format.json { head :no_content }
      end
    end

    # download the file
    # GET /drive/folders/1/files/1/download
    # GET /drive/files/1/download
    def download
      size = params[:size] ? params[:size].to_sym : :original

      if size == :original
        type = @file.upload_content_type
        ext  = @file.ext
      else
        type = 'image/jpeg'
        ext  = 'jpg'
      end

      send_file @file.upload.path(size), filename: @file.title + '.' + ext,
                type: type, disposition: 'inline'
    end


    private

      # find the containing folder
      def set_folder

        @folder = Folder.find params[:folder_id] unless params[:folder_id].nil?
        
      end

      # uploaded file may be in an array if file element had multiple set to true
      def fix_multiuploads

        


        return if params[:file].nil? || params[:file][:upload].nil?
        if params[:file][:upload].respond_to? :[]
          params[:file][:upload] = params[:file][:upload][0]
        end

        
      end

      # send alternate content type header for iframe transport
      def fix_iframe_header
        response.headers['Vary'] = 'Accept'
        if request.headers['Accept'] =~ /application\/json/
          response.headers['Content-Type'] = 'application/json'
        else
          response.headers['Content-Type'] = 'text/plain'
        end
      end

      def file_params
        params.require(:file).permit(:alt, :body, :folder_id, :title, :upload, :date)
      end

  end
end
