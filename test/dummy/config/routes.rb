Rails.application.routes.draw do
  
  # Only way to get engine routes working in a sub-directory, RackBaseURI wasn't working
  scope ENV['RAILS_RELATIVE_URL_ROOT'] || '/' do
    root to: 'home#index'
  
    get "home/index"
    get "home/activity", as: :activity
    get "home/pages", as: :pages 
  
    devise_for :users
    
    mount Drive::Engine => "/drive"
  end
end
