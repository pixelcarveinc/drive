#= require drive/namespace


class window.chocolate.drive.views.FileUploadRow extends Backbone.View

  tagName: 'tr'

  progressTemplate: JST['drive/file_upload_progress']
  failTemplate:     JST['drive/file_upload_fail']

  initialize: ->
    @uploader = @options.uploader
    @file     = @options.file
    @status   = 'progress'
    @percent  = 0

    @listenTo @uploader, 'done',     @done
    @listenTo @uploader, 'progress', @progress
    @listenTo @uploader, 'fail',     @fail

  render: ->
    switch @status
      when 'progress'
        @$el.html @progressTemplate
          file:     @file
          percent:  Math.round(@percent * 100)
      when 'fail'
        @$el.html @failTemplate file: @file
      when 'done'
        @$el.html @response

    @trigger 'render', @
    @

  progress: (up, e, data)->
    return unless @isThisFile(data)
    @percent = e.loaded / e.total
    @render()

  fail: (up, e, data) ->
    return unless @isThisFile(data)
    @status = 'fail'
    @render()

  done: (up, e, data)->
    return unless @isThisFile(data)
    @status = 'done'
    @response = $(data.result).children()
    @render()

  isThisFile: (data) ->
    data.files[0].name is @file.name
